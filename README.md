<p style="text-align: justify;">Cửa hàng Thuốc Tim Mạch trực thuộc Công ty Onplaza Việt Pháp chuyên cung cấp dòng sản phẩm An cung ngưu hoàng hoàn Hàn Quốc và Trung Quốc. Các sản phẩm của cửa hàng được đông đảo người tiêu dùng tin tưởng và sử dụng. Chúng tôi luôn cam kết mang lại những sản phẩm chất lượng nhất đến với khách hàng giúp chăm sóc sức khỏe người dùng hiệu quả nhất.</p>
<p style="text-align: justify;">- Sản phẩm <a href="https://thuoctimmach.vn/an-cung-nguu-han-quoc-cp502"><strong>An cung ngưu Hàn Quốc</strong></a> gồm:</p>

<ul>
 	<li style="text-align: justify;">Vũ Hoàng Thanh Tâm</li>
 	<li style="text-align: justify;">An cung ngưu tổ kén nội địa</li>
 	<li style="text-align: justify;">Bổ não hoàn hộp gỗ 60 viên</li>
 	<li style="text-align: justify;">Ngưu hoàng thanh tâm Liquid</li>
 	<li style="text-align: justify;">Kiện não hoàn hộp vàng, hộp xanh</li>
</ul>
<p style="text-align: center;"><img class="aligncenter" src="https://thuoctimmach.vn/anhtintuc/5/vu-hoang-thanh-tam-an-cung-han-quoc.jpg" /></p>
<p style="text-align: center;"><em>Sản phẩm Vũ Hoàng Thanh Tâm nổi tiếng của Hàn Quốc</em></p>
<p style="text-align: justify;">- Sản phẩm <a href="https://thuoctimmach.vn/an-cung-nguu-trung-quoc-cp503"><strong>An cung Trung Quốc</strong></a> nổi tiếng với các sản phẩm mang thương hiệu của Đồng Nhân Đường như:</p>

<ul>
 	<li style="text-align: justify;">An cung ngưu hộp thiếc đỏ 1 viên</li>
 	<li style="text-align: justify;">An cung ngưu hoàng đai vàng</li>
 	<li style="text-align: justify;">An cung ngưu thanh tâm tịnh chữ đen</li>
</ul>
<p style="text-align: center;"><img class="aligncenter" src="https://thuoctimmach.vn/Portals/2/1960252an-cung-nguu-hoang-hoan-hop-thiec-thuong-hang.jpg" /></p>
<p style="text-align: center;"><em>An cung ngưu hộp thiếc đỏ 1 viên do Đồng Nhân Đường sản xuất</em></p>
<p style="text-align: justify;">Ngoài ra còn có các sản phẩm như an cung ngưu rùa vàng, an cung ngưu đông á, hoa đà tái tạo hoàn....</p>
<p style="text-align: justify;">Tất cả các sản phẩm của Cửa hàng Thuốc Tim Mạch của Onplaza đều là hãng chính hãng, đạt chuẩn và được cấp phép lưu hành. Chúng tôi tự hào là đơn vị đi đầu trong ngành cung cấp các sản phẩm dinh dưỡng, thực phẩm chức năng giúp bảo vệ sức khỏe cho người dân.</p>
<p style="text-align: justify;">Cửa hàng Thuốc Tim Mạch có website chính thức là www.thuoctimmach.vn giúp khách hàng dễ dàng tham khảo về thông tin các sản phẩm một cách nhanh chóng và tiện lợi hoặc cần tư vấn thêm khách hàng có thể liên hệ theo thông tin:</p>

<ul>
 	<li style="text-align: justify;">- Địa chỉ: 327 Trường Chinh, Thanh Xuân, Hà Nội</li>
 	<li style="text-align: justify;">- Hotline: 0965.69.63.64</li>
 	<li style="text-align: justify;">- Số máy bàn: 02432.333.666</li>
 	<li style="text-align: justify;">- Gmail: thuoctimmach.vn@gmail.com</li>
 	<<li style="text-align: justify;">- Website: <a href="https://thuoctimmach.vn/">www.thuoctimmach.vn</a></li>
</ul>
